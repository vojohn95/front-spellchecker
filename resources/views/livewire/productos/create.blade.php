<!-- Modal -->
<div wire:ignore.self class="modal fade" id="createDataModal" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="createDataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createDataModalLabel">Create New Producto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="nombre">
                            <div class="spinner-grow" role="status" wire:loading>
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        </label>
                        <input wire:model.debounce.400ms="nombre" type="text" class="form-control" id="nombre"
                            placeholder="Nombre">
                        @if ($suggestionNom)
                            <a class="warning-text" wire:click='changenombre( {{ $suggestionNom }}, "nombre" )')>Tal vez
                                quiso decir: {{ $suggestionNom }}</a>
                        @endif
                        @error('nombre')
                            <span class="error text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="descripcion"></label>
                        <input wire:model.debounce.400ms="descripcion" type="text" class="form-control"
                            id="descripcion" placeholder="Descripcion">
                        @if ($suggestionDesc)
                            <a class="warning-text"
                                wire:click='changenombre( {{ $suggestionDesc }}, "descripcion" )')>Tal
                                vez
                                quiso decir: {{ $suggestionDesc }}</a>
                        @endif
                        @error('descripcion')
                            <span class="error text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="marca"></label>
                        <input wire:model.debounce.400ms="marca" type="text" class="form-control" id="marca"
                            placeholder="Marca">
                        @if ($suggestionMarca)
                            <a class="warning-text" wire:click='changenombre( {{ $suggestionMarca }}, "marca" )')>Tal
                                vez
                                quiso decir: {{ $suggestionMarca }}</a>
                        @endif
                        @error('marca')
                            <span class="error text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal">Save</button>
            </div>
        </div>
    </div>
</div>
