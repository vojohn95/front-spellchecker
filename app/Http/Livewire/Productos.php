<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Producto;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;


class Productos extends Component
{
    use LivewireAlert;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $nombre, $sku, $tamano, $descripcion, $marca, $suggestionNom, $suggestionDesc, $suggestionMarca;
    public $updateMode = false;

    public function render()
    {
        $keyWord = '%' . $this->keyWord . '%';
        return view('livewire.productos.view', [
            'productos' => Producto::latest()
                ->orWhere('nombre', 'LIKE', $keyWord)
                ->orWhere('sku', 'LIKE', $keyWord)
                ->orWhere('tamano', 'LIKE', $keyWord)
                ->orWhere('descripcion', 'LIKE', $keyWord)
                ->orWhere('marca', 'LIKE', $keyWord)
                ->paginate(10),
        ]);
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }

    private function resetInput()
    {
        $this->nombre = null;
        $this->sku = null;
        $this->tamano = null;
        $this->descripcion = null;
        $this->marca = null;
    }

    public function store()
    {
        // $this->validate([
        // ]);

        Producto::create([
            'nombre' => $this->nombre,
            'sku' => $this->sku,
            'tamano' => $this->tamano,
            'descripcion' => $this->descripcion,
            'marca' => $this->marca
        ]);

        $this->resetInput();
        $this->emit('closeModal');
        $this->alert('success', 'success', [
            'position' => 'top',
            'timer' => 3000,
            'toast' => true,
            'timerProgressBar' => true,
            'text' => '',
        ]);
    }

    public function edit($id)
    {
        $record = Producto::findOrFail($id);

        $this->selected_id = $id;
        $this->nombre = $record->nombre;
        $this->sku = $record->sku;
        $this->tamano = $record->tamano;
        $this->descripcion = $record->descripcion;
        $this->marca = $record->marca;

        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([]);

        if ($this->selected_id) {
            $record = Producto::find($this->selected_id);
            $record->update([
                'nombre' => $this->nombre,
                'sku' => $this->sku,
                'tamano' => $this->tamano,
                'descripcion' => $this->descripcion,
                'marca' => $this->marca
            ]);

            $this->resetInput();
            $this->updateMode = false;
            session()->flash('message', 'Producto Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Producto::where('id', $id);
            $record->delete();
        }
    }

    public function updatedNombre($value)
    {
        $this->spellChecker($value, 'nombre');
    }

    public function updatedDescripcion($value)
    {
        $this->spellChecker($value, 'descripcion');
    }

    public function updatedMarca($value)
    {
        $this->spellChecker($value, 'marca');
    }

    public function spellChecker($word, $campo)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://127.0.0.1:5000/checker/' . $word);
        $response = $request->getBody()->getContents();

        if ($campo == 'nombre') {
            $this->suggestionNom = $response;
        } elseif ($campo == 'descripcion') {
            $this->suggestionDesc = $response;
        } elseif ($campo == 'marca') {
            $this->suggestionMarca = $response;
        }
    }

    public function changenombre($value, $campo)
    {
        if ($campo == 'nombre') {
            $this->nombre = $value;
            $this->reset('suggestionNom');
        } elseif ($campo == 'descripcion') {
            $this->descripcion = $value;
            $this->reset('suggestionDesc');
        } elseif ($campo == 'marca') {
            $this->marca = $value;
            $this->reset('suggestionMarca');
        }
    }
}
